<?php
$set = true;
include('backbone/main.php');
include('templates/head.php');

$id = intval($_GET['id']);
$result = $db->query("SELECT * FROM `event` WHERE `id` = '$id' LIMIT 1");
if(@mysqli_num_rows($result)) {
	$event = mysqli_fetch_assoc($result);
	$ts = strtotime($event['expires']);
	if($ts < 1) $ts = time() - 1;
	if($event['expires'] == '0000-00-00 00:00:00' || time() < strtotime($event['expires'])) {
		if(empty($_POST))
			include('templates/event_signup.php');
		else {
			$name = !empty($_POST['name']) ? mysqli_real_escape_string($db, $_POST['name']) : null;
			$email = !empty($_POST['email']) ? mysqli_real_escape_string($db, $_POST['email']) : null;
			$phone = !empty($_POST['phone']) ? mysqli_real_escape_string($db, $_POST['phone']) : null;
			$address = !empty($_POST['address']) ? mysqli_real_escape_string($db, $_POST['address']) : null;
			if($db->query("INSERT INTO `event_signup` (`event_id`, `name`, `email`, `phone`, `address`) VALUES ('$id', '$name', '$email', '$phone', '$address')")) {
				echo '<h1>Success!</h1>';
				if(!empty($event['welcome'])) echo '<p>' . htmlspecialchars($event['welcome']) . '</p>';
			}
		}
	}
	else {
		echo 'The deadline has expired.';
	}
}
else {
	echo '<h1>;(</h1>';
}
include('templates/foot.php');