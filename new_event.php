<?php
$set = true;
include('backbone/main.php');
if(!$_SESSION['loggedin']) {header('Location: new_user.php');die();}
else {
	$error = false;
	if(!empty($_POST)) {
		if(!empty($_POST['eventname'])) {
			$eventname = mysqli_real_escape_string($db, $_POST['eventname']);
			$deadline = !empty($_POST['deadline']) ? mysqli_real_escape_string($db, date('Y-m-d H:i:s', strtotime($_POST['deadline']))) : null;
			// $name = (@$_POST['name'] == 'yup' ? '1' : '0');
			$mail = (@$_POST['mail'] == 'yup' ? '1' : '0');
			$phone = (@$_POST['phone'] == 'yup' ? '1' : '0');
			$address = (@$_POST['address'] == 'yup' ? '1' : '0');
			$eventaddress = !empty($_POST['eventaddress']) ? mysqli_real_escape_string($db, $_POST['eventaddress']) : null;
			$welcome = !empty($_POST['welcome']) ? mysqli_real_escape_string($db, $_POST['welcome']) : null;
			if($db->query("INSERT INTO `event` (`uid`, `eventname`, `address`, `get_name`, `get_email`, `get_phonenr`, `get_address`, `expires`, `welcome`)
					VALUES ('{$_SESSION['user']['uid']}', '$eventname', '$eventaddress', '1', '$mail', '$phone', '$address', '$deadline', '$welcome')")) {
				$id = mysqli_insert_id($db);
			}
			else $error = true;
		}
	}
	include('templates/head.php');
	if($error || (!empty($_POST) && empty($id))) echo '<div class="error">Something wrong :(<br>'.mysqli_error($db).'</div>';
	if(empty($id)) include('templates/sign_up.php'); else include('templates/eventlink.php');
	include('templates/foot.php');
}