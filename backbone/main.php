<?php
header('Content-type: text/html;Charset=UTF-8');
if(!isset($set)) die('This file is a component. It does not have web browser content.');
session_start();

$db = mysqli_connect('localhost', 'prealpha', 'test', 'prealpha')
	or die('Doesn\'t work ;( ... '
		. mysqli_error($db));

function login($db, $username, $password) {
	$user = mysqli_real_escape_string($db, $username);
	$result = $db->query("SELECT * FROM `users` WHERE `email` = '$user' LIMIT 1");
	if(@mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		if(password_verify($password, $row['password'])) {
			$_SESSION['user'] = $row;
			$_SESSION['loggedin'] = true;
			return true;
		}
		else return false;
	}
	else return false;
}

function username_exists($db, $username) {
	$user = mysqli_real_escape_string($db, $username);
	$result = mysqli_fetch_array($db->query("SELECT count(*) FROM `users` WHERE `username` = '$user'"));
	if($result[0] > 0) return true;
	else return false;
}

if(empty($_SESSION['user'])) {
	$usr['username'] = 'Gjest';
	$usr['loggedin'] = false;
	$_SESSION['user'] = $usr;
	$_SESSION['loggedin'] = false;
}