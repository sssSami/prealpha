-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2015 at 10:37 AM
-- Server version: 5.5.33
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prealpha`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `eventname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `get_name` tinyint(1) NOT NULL DEFAULT '0',
  `get_email` tinyint(1) NOT NULL DEFAULT '0',
  `get_phonenr` tinyint(1) NOT NULL DEFAULT '0',
  `get_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `welcome` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `uid`, `eventname`, `address`, `get_name`, `get_email`, `get_phonenr`, `get_address`, `info`, `expires`, `welcome`) VALUES
(4, 3, 'Redwood Trip', 'redwood National Park', 1, 0, 0, '0', '', '0000-00-00 00:00:00', ''),
(5, 3, 'Skog Folk', 'Skogen', 1, 1, 1, '1', '', '1970-01-01 01:00:00', 'Welcome Wilderness!'),
(6, 6, 'Hjemme Fest', 'Kristiansand gareveien 32', 1, 0, 1, '0', '', '2015-04-20 00:00:00', 'Welcome to the party!');

-- --------------------------------------------------------

--
-- Table structure for table `event_signup`
--

CREATE TABLE `event_signup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `event_signup`
--

INSERT INTO `event_signup` (`id`, `event_id`, `name`, `email`, `phone`, `address`) VALUES
(2, 4, 'Kay', '', '', ''),
(3, 4, 'bob', '', '', ''),
(4, 4, 'lisa', '', '', ''),
(5, 5, 'Jan Erik Olsen', 'JE@yahoo.com', '8403422347', 'Svingen til hÃ¸yre'),
(6, 5, 'Steffen Gamborg', 'seff-gam@live.no', '479628003', 'Bak det gule huset'),
(7, 5, 'Janne Gro Ellefsen', 'Janne87@gmail.com', '489279234', 'Rett ved naboen min'),
(8, 5, 'Magnus Kristiansen', 'Magga93@hotmail.com', '48929792', 'Husker ikke'),
(9, 5, 'Nicholas Robstad', 'Nico95@yahoo.com', '84739y432', 'Hos mamma'),
(10, 5, 'â€ŽBendiks Hjelleset', 'bendiks.hjelleset@gmail.com', '389830234', 'Bobilen utenfor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `tlf` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `email`, `tlf`, `reg`) VALUES
(2, 'k', '$2y$10$02CvuFfGfFrRQ148F0bTEuIB1N5R8hr0XzIeysYmcfRpDuQAP0zUy', 'k@k.no', '', '2015-03-15 07:12:47'),
(3, 'hans', '$2y$10$0l930KyUsd55jiLx3xvHw.09i3/IxqPIPXwECxGDggCq9KjGDPTJe', 'hans@hans.com', '', '2015-03-15 07:13:22'),
(4, 'jallaalla', '$2y$10$lzZ47.0KViXO331kqalZjuzyLEPf0QiKN869jKIOgQsCQCLAdK5CS', 'jallaalla@hotmail.com', '', '2015-03-15 08:59:30'),
(5, 'Erik', '$2y$10$OMmumNx6sC4jk4fmgvxyHuUZuw9xWfiTrwrRJ4.xcCoZrW6Go33B2', 'erik95@gmail.com', '', '2015-03-15 09:06:59'),
(6, 'Erik Olsen', '$2y$10$mhHhVQ2OfALhXvvWs8P3J.plK0OEIMcyoGre9oF42nqQTBDIeYXvS', 'erik95@hotmail.com', '', '2015-03-15 09:18:09');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
