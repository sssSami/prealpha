<?php
if(!$set) die();
?>

<form action="?signup=yes&amp;id=<?= intval($_GET['id']) ?>" method="post">
	<h1><?= $event['eventname'] ?></h1>
	<p><?= $event['address'] ?></p>
	<?= @$event['expires'] != '0000-00-00 00:00:00' ? '<p>Deadline: ' . $event['expires'] . '</p>' : null ?>
	<?= @$event['get_name'] == '1' ? '<p><input type="text" name="name" placeholder="Your name" required></p>' : null ?>
	<?= @$event['get_email'] == '1' ? '<p><input type="text" name="email" placeholder="Mail address" required></p>' : null ?>
	<?= @$event['get_phonenr'] == '1' ? '<p><input type="text" name="phone" placeholder="Phone number" required></p>' : null ?>
	<?= @$event['get_address'] == '1' ? '<p><input type="text" name="address" placeholder="Your address" required></p>' : null ?>
	<button type="submit">Sign me up!</button>
</form>