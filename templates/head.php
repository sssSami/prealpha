<!doctype html>
<html>
<head>
	<link rel="shortcut icon" href="">
    <meta name="viewport" content="width-device-width, initial-scale=1">
	<meta charset="utf-8" />
	<link rel="stylesheet" href="stylesheet.css" type="text/css" />	
	<link rel="stylesheet" href="responsive.css" type="text/css" media="screen and (max-width: 900px)"/>	
	<script src="js/jquery.js"></script>
</head>
<body>

<div class="menubarframe">
	<div class="menubarframe2">
   		<div style="float: left;"><img src="img/hackathon_app_logo.png" style=" height:50px; padding-top:5px;"></div>
    	<div class="menubar">
		<?= @$_SESSION['loggedin'] ? '<div><a href="new_event.php">Create event</a></div>' : null ?>
		<div><?= @$_SESSION['loggedin'] ? '<a href="logout.php">Log out</a>' : '<a href="login.php">Log in</a>' ?></div>
		<div><a href="index.php">Home</a></div>
		</div>
	</div>
</div>

<?= !isset($nowrapper) ? '<div id="wrapper">' : null ?>