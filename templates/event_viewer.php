<h1><?= htmlspecialchars($event['eventname']) ?></h1>
<p><?= htmlspecialchars($event['address']) ?></p>
<div id="event_link"><p>URL: http://<?= dirname($_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) ?>/event.php?id=<?= $event['id'] ?></p></div>
<h1>Participants</h1>
<?php if(@mysqli_num_rows($query)) { ?>
<table>
<tr><th>Name</th><th>E-mail</th><th>Phone</th><th>Address</th></tr>
<?php
while($row = mysqli_fetch_assoc($query)) {
	echo '<tr>';
	
	echo '<td>';
	echo htmlspecialchars($row['name']);
	echo '</td>';
	if($event['get_email']) {
		echo '<td>';
		echo htmlspecialchars($row['email']);
		echo '</td>';
	} if($event['get_phonenr']){
		echo '<td>';
		echo htmlspecialchars($row['phone']);
		echo '</td>';
	} if($event['get_address']) {
		echo '<td>';
		echo htmlspecialchars($row['address']);
		echo '</td>';
	}
	echo '</tr>';
}
}
else echo '<p>No participants</p>';
?>
</table>